use std::sync::Mutex;
use crate::file_vec::FileVec;
use std::path;
use std::io;
use serde::{Deserialize, Serialize};
use chrono::{DateTime, FixedOffset};

#[derive(Serialize, Deserialize, Clone, Debug, PartialOrd, PartialEq, Eq, Ord, Hash)]
pub struct CalendarEntry {
    pub id: u32,
    pub start: DateTime<FixedOffset>,
    pub end: DateTime<FixedOffset>,
    pub title: String,
    pub description: Option<String>,
    pub location: Option<String>,
}

pub struct CalendarVec {
    pub mutex: Mutex<FileVec<CalendarEntry>>
}

impl CalendarVec {
    #[inline]
    pub fn new<P: std::clone::Clone + AsRef<path::Path>>(path: P) -> CalendarVec {
        CalendarVec {
            mutex: Mutex::new(FileVec::new(path).unwrap()),
        }
    }

    #[inline]
    pub fn push(&self, entry: CalendarEntry) -> io::Result<()> {
        self.mutex.lock().unwrap().push(entry)
    }

    #[inline]
    pub fn delete(&self, id: u32) -> io::Result<()> {
        self.mutex.lock().unwrap().retain(|entry| entry.id == id)
    }

    pub fn next_id(&self) -> u32 {
        if self.mutex.lock().unwrap().vec.is_empty() {
            return 1u32;
        } else {
            for (index, entry) in self.mutex.lock().unwrap().vec.iter().enumerate() {
                let possible_id = (index + 1) as u32;
                if entry.id != possible_id {
                    return possible_id;
                }
            }
        }

        (self.mutex.lock().unwrap().vec.len() + 1) as u32
    }
}