use std::fs;
use std::path;
use std::io;
use serde::{Serialize};
use serde::de::DeserializeOwned;
use std::io::{Read, ErrorKind};
use crate::elevate;

pub struct FileVec<T> where T: Serialize + DeserializeOwned {
    pub(crate) vec: Vec<T>,
    pub(crate) path: String,
}

impl<T> FileVec<T> where T: Serialize + DeserializeOwned {
    pub fn new<P: std::clone::Clone + AsRef<path::Path>>(path: P) -> io::Result<FileVec<T>> {
        let mut content = String::new();
        if path.as_ref().exists() {
            fs::File::with_options().read(true).create(true).open(path.clone())?.read_to_string(&mut content)?;

            Ok(FileVec {
                vec: serde_json::from_str(&*content)?,
                path: String::from(path.as_ref().to_str().unwrap()),
            })
        } else {
            if let Some(parent) = path.as_ref().parent() {
                if !parent.exists() {
                    fs::create_dir_all(parent)?;
                }
            }

            match fs::File::create(path.clone()) {
                Ok(_) => {
                    let v = FileVec {
                        vec: Vec::new(),
                        path: String::from(path.as_ref().to_str().unwrap()),
                    };
                    v.save_entries()?;
                    Ok(v)
                },
                Err(err) => {
                    if err.kind() == ErrorKind::PermissionDenied {
                        elevate();
                    } else {
                        Err(err)
                    }
                }
            }
        }

    }
    pub fn push(&mut self, entry: T) -> io::Result<()> {
        self.vec.push(entry);
        self.save_entries()?;
        Ok(())
    }
    pub fn retain<F>(&mut self, f: F) -> io::Result<()> where F: FnMut(&T) -> bool {
        self.vec.retain(f);
        self.save_entries()?;
        Ok(())
    }
    pub fn save_entries(&self) -> io::Result<()> {
        let file = fs::OpenOptions::new().write(true).append(false).read(true).open(path::Path::new(&self.path))?;
        serde_json::to_writer(file, &self.vec)?;
        Ok(())
    }
}