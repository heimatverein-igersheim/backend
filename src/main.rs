#![feature(proc_macro_hygiene, decl_macro)]
#![feature(with_options)]

#[macro_use] extern crate rocket;

use chrono::prelude::*;
use serde::{Deserialize, Serialize};
use rocket::{State};
use std::vec::Vec;

use rocket_contrib::json::Json;

use std::process::{Command, Stdio};

use rocket::http::RawStr;
use chrono::Duration;

pub mod auth;
pub mod file_vec;
pub mod user;
pub mod calendar;

use user::User;

use calendar::{CalendarEntry, CalendarVec};
use crate::auth::{AccessToken, TokenVec};
use crate::user::UserVec;

#[derive(Debug, Clone, Copy, Hash, Eq, Ord, PartialOrd, PartialEq, Serialize, Deserialize)]
pub struct RequestError<'a> {
    error: &'a str
}

#[derive(Serialize, Deserialize)]
pub struct LoginResponse<'a> {
    success: bool,
    access_token: Option<&'a str>,
}

#[get("/")]
fn index<'a>() -> &'a str {
    "Hello, world!"
}

#[post("/login?<username>&<password>")]
fn login<'a>(tokens: State<TokenVec>, users: State<UserVec>, username: String, password: String) -> Json<LoginResponse<'a>> {
    match users.get_by_username(username) {
        Some(user) => {
            if user.is_password_valid(password) {
                let token = AccessToken::generate_new(user.clone(), DateTime::from(Utc::now() + Duration::weeks(2)));
                tokens.push(token).unwrap();
                Json(LoginResponse {
                    success: true,
                    access_token: Some(Box::leak(token.to_string().into_boxed_str()))
                })
            } else {
                Json(LoginResponse {
                    success: false,
                    access_token: Option::None,
                })
            }
        }
        None => {
            Json(LoginResponse {
                success: false,
                access_token: Option::None,
            })
        }
    }
}

#[post("/calendar/entry?<start>&<end>&<title>&<description>&<location>")]
fn add_entry<'a>(_token: AccessToken, calendar: State<CalendarVec>, start: String, end: String, title: String, description: Option<String>, location: Option<String>) -> Result<(), RequestError<'a>> {
    if let Ok(start) = DateTime::parse_from_rfc3339(&start) {
        if let Ok(end) = DateTime::parse_from_rfc3339(&end) {
            let entry = CalendarEntry {
                id: calendar.next_id(), // auto increment
                start,
                end,
                title,
                description,
                location,
            };
            calendar.push(entry).unwrap();
            Ok(())
        } else {
            return Err(RequestError {
                error: "Invalid `end` format",
            });
        }
    } else {
        return Err(RequestError {
            error: "Invalid `start` format",
        });
    }
}

#[delete("/calendar/entry?<id>")]
fn delete_entry<'a>(_token: AccessToken, calendar: State<CalendarVec>, id: Result<u32, &RawStr>) -> Result<(), RequestError<'a>> {

    match id {
        Ok(id) => {
            calendar.delete(id).unwrap();
            Ok(())
        },
        Err(_) => {
            Err(RequestError {
                error: "Invalid u32",
            })
        }
    }
}

#[get("/calendar/entries", rank = 3)]
fn all_calendar_entries(calendar: State<CalendarVec>) -> Json<Vec<CalendarEntry>> {
    Json(calendar.mutex.lock().unwrap().vec.clone())
}

#[get("/calendar/entries?<month>&<year>", rank = 1)]
fn calendar_entries<'a>(calendar: State<CalendarVec>, month: Result<u32, &RawStr>, year: Result<u32, &RawStr>) -> Json<Result<Vec<CalendarEntry>, RequestError<'a>>> {
    match month {
        Ok(month) => {
            match year {
                Ok(year) => {
                    let lock = calendar.mutex.lock().unwrap();
                    let resp = lock.vec.iter()
                        .filter(|&entry| entry.start.month() == month && (entry.start.year() as u32) == year);
                    let iterator = resp.into_iter();
                    let mut new_vec: Vec<CalendarEntry> = Vec::new();
                    for entry in iterator {
                        new_vec.push(entry.clone());
                    }
                    Json(Ok(new_vec))
                },
                Err(_) => {
                    Json(Err(RequestError {
                        error: "Invalid u32",
                    }))
                }
            }
        },
        Err(_) => {
            Json(Err(RequestError {
                error: "Invalid u32",
            }))
        }
    }
}

#[get("/calendar/entries?<count>", rank = 2)]
fn get_next_entries<'a>(calendar: State<CalendarVec>, count: Result<usize, &RawStr>) -> Json<Result<Vec<CalendarEntry>, RequestError<'a>>> {
    match count {
        Ok(count) => {
            let mut vec = calendar.mutex.lock().unwrap().vec.clone();
            vec.sort_by(|i, j|i.start.cmp(&j.start));
            let now = Utc::now();
            let resp: Vec<&CalendarEntry> = vec.iter().filter(|&entry| now >= entry.start).take(count).collect();
            let mut new_vec: Vec<CalendarEntry> = Vec::with_capacity(count);
            for entry in resp {
                new_vec.push(entry.clone());
            }
            Json(Ok(new_vec))
        },
        Err(_) => {
            Json(Err(RequestError {
                error: "Invalid u32",
            }))
        }
    }
}

fn main() {
    let token_vec = TokenVec::new(Vec::new());
    let user_vec: UserVec = UserVec::new("./config/users.json");
    if user_vec.mutex.lock().unwrap().vec.is_empty() {
        user_vec.create("dev".to_string(), "Papierkorb").expect("Cannot create user `dev`");
        user_vec.create("jascha.derr".to_string(), "gg020gmwqfq$!$140\\").expect("Cannot create user `jascha.derr`");
    }
    let calendar = CalendarVec::new("./config/calendar.json");
    rocket::ignite().manage(token_vec).manage(user_vec).manage(calendar).mount("/", routes![index, login, all_calendar_entries, get_next_entries, calendar_entries, delete_entry, add_entry]).launch();
}

#[cfg(target_os = "windows")]
pub fn elevate() -> ! {
    Command::new("start")

        .args(&["runas", "/user:Administrator", std::env::current_exe().unwrap().to_str().unwrap()])
        .stdout(Stdio::null())
        .stdin(Stdio::null())
        .stderr(Stdio::null())
        .spawn()
        .expect("failed to execute process");

    std::process::exit(0x5) // access denied
}

#[cfg(target_os = "linux")]
pub fn elevate() -> ! {
    std::process::exit(1) // access denied
}