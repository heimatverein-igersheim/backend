use serde::{Deserialize, Serialize};

use std::sync::Mutex;
use crate::file_vec::FileVec;
use std::io;

#[derive(Serialize, Deserialize, Clone, Debug, PartialOrd, PartialEq, Eq, Ord, Hash)]
pub struct User {
    pub id: u32,
    pub username: String,
    pub password: Vec<u8>,
}

impl User {
    pub fn new(id: u32, username: String, password: EncryptedPassword) -> User {
        User {
            id,
            username,
            password: password.vec,
        }
    }

    pub fn is_password_valid(&self, cmp: String) -> bool {
        EncryptedPassword { vec: self.password.clone() }.decrypt() == cmp
    }
}

pub struct EncryptedPassword {
    vec: Vec<u8>,
}

impl EncryptedPassword {
    pub fn new(password: &str) -> EncryptedPassword {
        let mut vec: Vec<u8> = Vec::with_capacity(password.len());

        for b in password.bytes() {
            if b < 255 {
                vec.push(b + 1)
            } else {
                vec.push(0)
            }
        }

        EncryptedPassword {
            vec
        }
    }

    pub fn decrypt<'a>(self) -> &'a str {
        let mut vec: Vec<u8> = Vec::with_capacity(self.vec.len());
        for b in self.vec {
            if b > 0 {
                vec.push(b - 1);
            } else {
                vec.push(255);
            }
        }
        let s = String::from_utf8_lossy(vec.as_slice());
        Box::leak(s.into_owned().into_boxed_str())
    }
}

pub struct UserVec {
    pub mutex: Mutex<FileVec<User>>
}

impl UserVec {
    #[inline]
    pub fn new<P: std::clone::Clone + AsRef<std::path::Path>>(path: P) -> UserVec {
        UserVec {
            mutex: Mutex::new(FileVec::new(path).unwrap()),
        }
    }

    #[inline]
    pub fn create(&self, username: String, password: &str) -> io::Result<()> {
        self.push(User::new(self.next_id(),username, EncryptedPassword::new(password)))
    }

    #[inline]
    pub fn get_by_username(&self, username: String) -> Option<User> {
        self.mutex.lock().unwrap().vec.iter().find(|&user| user.username.eq_ignore_ascii_case(&*username)).cloned()
    }

    #[inline]
    pub fn get_by_id(&self, id: u32) -> Option<User> {
        self.mutex.lock().unwrap().vec.iter().find(|&user| user.id == id).cloned()
    }

    #[inline]
    pub fn push(&self, entry: User) -> io::Result<()> {
        self.mutex.lock().unwrap().push(entry)
    }

    #[inline]
    pub fn delete(&self, id: u32) -> io::Result<()> {
        self.mutex.lock().unwrap().retain(|entry| entry.id == id)
    }

    pub fn next_id(&self) -> u32 {
        if self.mutex.lock().unwrap().vec.is_empty() {
            return 1u32;
        } else {
            for (index, entry) in self.mutex.lock().unwrap().vec.iter().enumerate() {
                let possible_id = (index + 1) as u32;
                if entry.id != possible_id {
                    return possible_id;
                }
            }
        }

        (self.mutex.lock().unwrap().vec.len() + 1) as u32
    }
}