use chrono::{DateTime, FixedOffset, Utc};
use serde::{Deserialize, Serialize};
use crate::{User};
use std::io;
use rand::{thread_rng, Rng};
use rocket::{Outcome, State};
use rocket::http::Status;
use rocket::request::{self, Request, FromRequest};
use std::sync::Mutex;

const B32_ALPHABET: base32::Alphabet = base32::Alphabet::Crockford;

#[derive(Ord, PartialOrd, Eq, PartialEq, Hash, Copy, Clone)]
pub struct AccessToken {
    token: [u8; 32],
    user_id: u32,
    valid_until: DateTime<FixedOffset>,
}

impl AccessToken {
    pub fn generate_new(user: User, valid_until: DateTime<FixedOffset>) -> AccessToken {
        let mut rng = thread_rng();
        let bytes = mac_address::get_mac_address().unwrap().unwrap().bytes();
        let mut token_bytes: Vec<u8> = Vec::with_capacity(32);

        let id_bytes: [u8; 4] = unsafe { std::mem::transmute(user.id) };

        // random byte
        token_bytes.push(rng.gen());

        // mac address
        token_bytes.push(bytes[0] ^ bytes[1] ^ bytes[2]);
        token_bytes.push(bytes[3] & id_bytes[0]);
        token_bytes.push(bytes[4] ^ id_bytes[1]);
        token_bytes.push(bytes[5] | id_bytes[2]);

        for _ in 6..=32 {
            token_bytes.push(rng.gen());
        }

        let mut array = [0; 32];
        let bytes = &token_bytes[..];
        array.copy_from_slice(bytes);

        AccessToken {
            token: array,
            user_id: user.id,
            valid_until
        }
    }

    pub fn is_valid(&self) -> bool {
        let now = Utc::now();
        self.valid_until.naive_utc() < now.naive_utc()
    }

    pub fn of(data: &str, user: User, valid_until: DateTime<FixedOffset>) -> Result<AccessToken, TokenError>{
        match base32::decode(B32_ALPHABET, data) {
            Some(data) => {
                if data.len() == 32 {
                    let verify_bytes = mac_address::get_mac_address().unwrap().unwrap().bytes();
                    let id_bytes: [u8; 4] = unsafe { std::mem::transmute(user.id) };
                    if (data[1] == verify_bytes[0] ^ verify_bytes[1] ^ verify_bytes[2]) &&
                        (data[2] == verify_bytes[3] & id_bytes[0]) &&
                        (data[3] == verify_bytes[4] ^ id_bytes[1]) &&
                        (data[4] == verify_bytes[5] | id_bytes[2]) {

                        let mut array = [0; 32];
                        let bytes = &data[..];
                        array.copy_from_slice(bytes);

                        Ok(AccessToken {
                            token: array,
                            user_id: user.id,
                            valid_until,
                        })
                    } else {
                        Err(TokenError::InvalidSignature)
                    }
                } else {
                    Err(TokenError::InvalidLength(data.len()))
                }
            },
            None => {
                Err(TokenError::DecodeError)
            }
        }
    }
}

impl<'a> ToString for AccessToken {
    fn to_string(&self) -> String {
        base32::encode(B32_ALPHABET, &self.token)
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for AccessToken {
    type Error = TokenError;

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        return if let Outcome::Success(vec) = request.guard::<State<TokenVec>>() {
            if let Some(auth_header) = request.headers().get_one("Authentication") {
                if let Some(auth_token) = auth_header.strip_prefix("Bearer ") {
                    match vec.get_by_token(auth_token) {
                        Ok(access_token) => {
                            if access_token.is_valid() {
                                Outcome::Success(access_token)
                            } else {
                                Outcome::Failure((Status::BadRequest, TokenError::Expired))
                            }
                        },
                        Err(err) => Outcome::Failure((Status::BadRequest, err))
                    }
                } else {
                    Outcome::Failure((Status::BadRequest, TokenError::DecodeError))
                }
            } else {
                Outcome::Failure((Status::BadRequest, TokenError::DecodeError))
            }
        } else {
            Outcome::Failure((Status::BadRequest, TokenError::DecodeError))
        }
    }
}

#[derive(Debug, Clone, Copy, Hash, Ord, PartialOrd, Eq, PartialEq, Serialize, Deserialize)]
pub enum TokenError {
    InvalidLength(usize),
    InvalidSignature,
    Expired,
    NotExists,
    DecodeError,
}

impl std::fmt::Display for TokenError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match *self {
            TokenError::InvalidLength(len) => write!(f, "Invalid token length ({})", len),
            TokenError::InvalidSignature => write!(f, "Invalid token signature"),
            TokenError::Expired => write!(f, "Token is already expired."),
            TokenError::NotExists => write!(f, "Token does not exist"),
            TokenError::DecodeError => write!(f, "Base32 decoding failed")
        }
    }
}

pub struct TokenVec {
    mutex: Mutex<Vec<AccessToken>>,
}

impl<'a> TokenVec {
    pub fn new(vec: Vec<AccessToken>) -> TokenVec {
        TokenVec {
            mutex: Mutex::new(vec),
        }
    }

    pub fn push(&self, token: AccessToken) -> io::Result<()> {
        self.mutex.lock().unwrap().push(token);
        Ok(())
    }

    pub fn get_by_token(&self, token: &'a str) -> Result<AccessToken, TokenError> {
        let mut vec = self.mutex.lock().unwrap();
        match vec.iter().find(|&t| t.to_string() == token) {
            Some(&t) => {
                if t.is_valid() {
                    Ok(t)
                } else {
                    vec.retain(|&x| x != t);
                    Err(TokenError::Expired)
                }
            },
            None => Err(TokenError::NotExists)
        }
    }
}