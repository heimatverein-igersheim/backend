#[cfg(windows)]
extern crate winres;

#[cfg(windows)]
extern crate svg_to_ico;

use std::path::Path;

fn main() {
    if cfg!(target_os = "windows") {
        let input = Path::new("./icon.svg");
        let output_path = "./target/icon.ico";
        let output = Path::new(output_path);

        svg_to_ico::svg_to_ico(input, 96.0, output, &[32, 48, 64, 128, 256]).expect("Failed whilst converting .svg to .ico");

        let mut res = winres::WindowsResource::new();
        res.set_icon(output_path);
        res.compile().unwrap();
    }
}

#[cfg(unix)]
fn main() {
}